<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'Api\Auth\AuthContrloller@login');
    Route::post('register', 'Api\Auth\AuthContrloller@signup');
    Route::post('reset', 'Api\Auth\AuthContrloller@reset');

    Route::group(['middleware' => ['auth:api']], function() {
        Route::get('logout', 'Api\Auth\AuthContrloller@logout');
    });
});

Route::group(['prefix' => 'employee', 'middleware' => ['auth:api']], function () {
    Route::get('getlistemployee', 'Api\Employee\EmployeeContrloller@getemployee');
    Route::post('addemployee', 'Api\Employee\EmployeeContrloller@AddEmployee');
    Route::get('editemployee', 'Api\Employee\EmployeeContrloller@UpdateEmployee');
    Route::get('deleteemployee/{id}', 'Api\Employee\EmployeeContrloller@DeleteEmployee');
    Route::get('searchemployee', 'Api\Employee\EmployeeContrloller@Searchemployee');
});
