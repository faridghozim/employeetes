<?php

namespace App\Http\Controllers\Api\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\employee;
use Auth;
use DB;

class EmployeeContrloller extends Controller
{
   /**
     * @OA\Get(
     *     path="/employee/getlistemployee",
     *      @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required=true,
     *         description="Bearer {access-token}",
     *         @OA\Schema(
     *              type="bearerAuth"
     *         ) 
     *      ), 
     *     @OA\Response(response="200", description="list all employ using pagination and Redis cache")
     * )
     */
    public function getemployee()
    {
        $currentPage = request()->get('page',1);
        $itemperpage = request()->get('itemperpage',10);
        $query = Cache::remember('sellcategory-' . $currentPage, 10, function() use ($itemperpage) {
            return DB::table('employee')->paginate($itemperpage);
        });

        return response()->json(['results' => $query], 200);
    }

    /**
     * @OA\Post(
     *     path="/employee/addemployee",
     *      @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required=true,
     *         description="Bearer {access-token}",
     *         @OA\Schema(
     *              type="bearerAuth"
     *         ) 
     *      ), 
     *     @OA\Parameter(
     *        name="full_name",
     *        in="path",
     *        description="Full name empolyee with type data string",
     *        required=true,
     *      ),
     *     @OA\Parameter(
     *        name="nick_name",
     *        in="path",
     *        description="Short name employee with type data string",
     *        required=true,
     *      ),
     *     @OA\Parameter(
     *        name="age",
     *        in="path",
     *        description="Age of employee with type data integer",
     *        required=true,
     *      ),
     *     @OA\Parameter(
     *        name="birth_date",
     *        in="path",
     *        description="Birt date of employee with format yyyy-mm-dd",
     *        required=true,
     *      ),
     *     @OA\Parameter(
     *        name="address",
     *        in="path",
     *        description="String Address of employee, ",
     *        required=true,
     *      ),
     *     @OA\Parameter(
     *        name="mobile",
     *        in="path",
     *        description="Mobile phone of employee with type data string",
     *        required=true,
     *      ),
     *     @OA\Parameter(
     *        name="avatar",
     *        in="path",
     *        description="Avatar of employee",
     *        required=false,
     *      ),
     *     @OA\Response(response="200", description="Success Add employee")
     * )
     */
    public function AddEmployee(Request $request){
        $request->validate([
            'full_name' => 'required|string',
            'nick_name' => 'required|string',
            'age' => 'required|integer',
            'birth_date' => 'required|string',
            'address' => 'required|string',
            'mobile' => 'required|string'
          ]);

        $user_id = Auth::id();
        $data = ['full_name'=> $request->full_name,
                'nick_name'=>$request->nick_name,
                'age'=>$request->age,
                'birth_date'=>$request->birth_date,
                'address'=>$request->address,
                'mobile'=>$request->mobile,
                'avatar'=>$request->avatar,
                'create_by' => $user_id,
                'modify_by' => $user_id,
                ];

        $employee = new employee();// buat employee baru
        $employee->fill($data);// isi employee dari data excel
        $employee->save(); // simpan employee

        return response()->json(['message' => 'Success Add employee'], 200);
    }

    /**
     * @OA\Get(
     *     path="/employee/editemployee",
     *      @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required=true,
     *         description="Bearer {access-token}",
     *         @OA\Schema(
     *              type="bearerAuth"
     *         ) 
     *      ), 
     *     @OA\Parameter(
     *        name="id",
     *        in="path",
     *        description="id employee will be edit data",
     *        required=true,
     *      ),
     *     @OA\Parameter(
     *        name="full_name",
     *        in="path",
     *        description="Full name empolyee with type data string",
     *        required=true,
     *      ),
     *     @OA\Parameter(
     *        name="nick_name",
     *        in="path",
     *        description="Short name employee with type data string",
     *        required=true,
     *      ),
     *     @OA\Parameter(
     *        name="age",
     *        in="path",
     *        description="Age of employee with type data integer",
     *        required=true,
     *      ),
     *     @OA\Parameter(
     *        name="birth_date",
     *        in="path",
     *        description="Birt date of employee with format yyyy-mm-dd",
     *        required=true,
     *      ),
     *     @OA\Parameter(
     *        name="address",
     *        in="path",
     *        description="String Address of employee, ",
     *        required=true,
     *      ),
     *     @OA\Parameter(
     *        name="mobile",
     *        in="path",
     *        description="Mobile phone of employee with type data string",
     *        required=true,
     *      ),
     *     @OA\Parameter(
     *        name="avatar",
     *        in="path",
     *        description="Avatar of employee",
     *        required=false,
     *      ),
     *     @OA\Parameter(
     *        name="modify_by",
     *        in="path",
     *        description="id user who moodify of employee data",
     *        required=true,
     *      ),
     *     @OA\Response(response="400", description="failed update employee, employee data not found."),
     *     @OA\Response(response="200", description="Success update employee")
     * )
     */
    public function UpdateEmployee(Request $request){
        $request->validate([
            'id' => 'required|integer',
            'full_name' => 'required|string',
            'nick_name' => 'required|string',
            'age' => 'required|integer',
            'birth_date' => 'required|string',
            'address' => 'required|string',
            'mobile' => 'required|string',
            'modify_by' => 'required|integer'
          ]);

        $data = employee::where('id',$request->id)->first();
        
        
        if($data != null){
            $data->full_name = $request->full_name;
            $data->nick_name = $request->nick_name;
            $data->age = $request->age;
            $data->birth_date = $request->birth_date;
            $data->address = $request->address;
            $data->mobile = $request->mobile;
            $data->avatar = $request->avatar;
            $data->create_by = $data->create_by;
            $data->modify_by = $request->modify_by;
            $data->save();
            return response()->json(['message' => 'Success update employee'], 200);
        }else{
            return response()->json(['message' => 'failed update employee, employee data not found'], 400);
        }
    }

     /**
     * @OA\Get(
     *     path="/employee/deleteemployee/{id}",
     *      @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required=true,
     *         description="Bearer {access-token}",
     *         @OA\Schema(
     *              type="bearerAuth"
     *         ) 
     *      ), 
     *     @OA\Parameter(
     *        name="id",
     *        in="path",
     *        description="id employee will be delete data",
     *        required=true,
     *      ),
     *     @OA\Response(response="200", description="Success Delete employee"),
     *     @OA\Response(response="400", description="failed Delete employee, employee data not found")
     * )
     */
    public function DeleteEmployee($id){
        $employee = employee::find($id);

        if($data != null){
            $employee->delete();
            return response()->json(['message' => 'Success Delete employee'], 200);
        }else{
            return response()->json(['message' => 'failed Delete employee, employee data not found'], 400);
        }
    }

    /**
     * @OA\Get(
     *     path="/employee/searchemployee",
     *      @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required=true,
     *         description="Bearer {access-token}",
     *         @OA\Schema(
     *              type="bearerAuth"
     *         ) 
     *      ), 
     *     @OA\Parameter(
     *        name="search",
     *        in="path",
     *        description="Search employee by name using like in database",
     *        required=true,
     *      ),
     *     @OA\Response(response="200", description="list of employee with filtering by name")
     * )
     */
    public function Searchemployee(Request $request)
    {
        $search = $request->search;
        $query = Cache::remember('search-'.$search , 60, function () use ($search) {
            return DB::table('employee')
                        ->Where('full_name', 'like', '%' . $search . '%')
                        ->paginate(15);
        });

        return response()->json(['results' => $query], 200);
    }
}
