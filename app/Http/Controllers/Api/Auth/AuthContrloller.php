<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use Hash;
use DB;
use OpenApi;

class AuthContrloller extends Controller
{
    /**
     * @OA\Post(
     *     path="/auth/register",
     *     @OA\Parameter(
     *        name="name",
     *        in="path",
     *        description="Username of user",
     *        required=true,
     *      ),
     *     @OA\Parameter(
     *        name="email",
     *        in="path",
     *        description="Email addres of user",
     *        required=true,
     *      ),
     *     @OA\Parameter(
     *        name="password",
     *        in="path",
     *        description="Password of user",
     *        required=true,
     *      ),
     *     @OA\Response(response="201", description="message success create user.")
     * )
     */
    public function signup(Request $request)
    {
        $request->validate([
          'name' => 'required|string',
          'email' => 'required|string',
          'password' => 'required|string'
      ]);
        $user = new User([
          'name' => $request->name,
          'email' => $request->email,
          'password' => bcrypt($request->password)
      ]);
        $user->save();
        return response()->json([
          'message' => 'Successfully created user!'
      ], 201);
    }

    /**
     * @OA\Post(
     *     path="/auth/login",
     *     @OA\Parameter(
     *        name="email",
     *        in="path",
     *        description="Email addres of user",
     *        required=true,
     *      ),
     *     @OA\Parameter(
     *        name="password",
     *        in="path",
     *        description="Password of user",
     *        required=true,
     *      ),
     *     @OA\Parameter(
     *        name="remember_me",
     *        in="path",
     *        description="remember of user login with value true or false",
     *        required=true,
     *      ),
     *     @OA\Response(response="400", description="Unauthorized user."),
     *     @OA\Response(response="200", description="User token.")
     * )
     */
    public function login(Request $request)
    {
        $request->validate([
          'email' => 'required|string',
          'password' => 'required|string',
          'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);
        if (!Auth::attempt($credentials)) {
            return response()->json([
              'message' => 'Unauthorized'
            ], 401);
        }
        $user = $request->user();
        
        $tokenResult = $user->createToken('naystech');
        $token = $tokenResult->token;
        if ($request->remember_me) {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }
        $token->save();
        return response()->json([
            'name' => $user->name,
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
        
    }

    /**
     * @OA\Get(
     *     path="/auth/logout",
     *      @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required=true,
     *         description="Bearer {access-token}",
     *         @OA\Schema(
     *              type="bearerAuth"
     *         ) 
     *      ), 
     *     @OA\Response(response="200", description="Message Successfully logged out")
     * )
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
          'message' => 'Successfully logged out'
      ]);
    }
    
/**
     * @OA\Post(
     *     path="/auth/reset",
     *     @OA\Parameter(
     *        name="email",
     *        in="path",
     *        description="Email addres of user",
     *        required=true,
     *      ),
     *     @OA\Parameter(
     *        name="password",
     *        in="path",
     *        description="Password of user",
     *        required=true,
     *      ),
     *     @OA\Response(response="400", description="Failed Reset Password."),
     *     @OA\Response(response="200", description="Success Reset Password")
     * )
     */
    public function reset(Request $request){
        $results = User::where('email', $request->email)->first();
        if($results != null){
            $results->name= $results->name;
            $results->email= $results->email;
            $results->password=bcrypt($request->password);
            $results->save();
            return response()->json([
                'message' => 'Success Reset Password!'
              ], 200);
        }else{
            return response()->json([
                'message' => 'Failed Reset Password!'
            ], 401);
        }
    }
}
