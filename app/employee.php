<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class employee extends Model
{
    use Notifiable, SoftDeletes;
    
    protected $table = "employee";

    protected $dates = ['deleted_at'];

    //
    protected $fillable = ['full_name',
    'nick_name',
    'age',
    'birth_date',
    'address',
    'mobile',
    'avatar',
    'create_by',
    'modify_by',
    'delete_at'];
}
