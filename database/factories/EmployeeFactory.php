<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\employee;

$factory->define(employee::class, function (Faker $faker) {
    return [
        //
        'full_name' => $faker->name,
        'nick_name' => $faker->firstName,
        'age' => $faker->numberBetween(25,40),
        'birth_date' => $faker->dateTimeThisCentury->format('Y-m-d'), // birthdate
        'address' => $faker->address,
        'mobile' => $faker->tollFreePhoneNumber,
        'avatar' => $faker->imageUrl($width = 640, $height = 480),
        'create_by' => $faker->numberBetween(1,3),
        'modify_by' => $faker->numberBetween(1,3)
    ];
});
