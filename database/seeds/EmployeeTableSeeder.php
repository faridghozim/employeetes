<?php

use Illuminate\Database\Seeder;
use App\employee;

class EmployeeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $count = 100;
        factory(employee::class, $count)->create();
    }
}
